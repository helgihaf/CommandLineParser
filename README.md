# Marsonsoft.CommandLineParser [![Build status](https://ci.appveyor.com/api/projects/status/f8hltahpcdof0061?svg=true)](https://ci.appveyor.com/project/helgihaf/commandlineparser) [![NuGet](https://badge.fury.io/nu/Marsonsoft.CommandLineParser.svg)](https://www.nuget.org/packages/Marsonsoft.CommandLineParser) [![License](https://img.shields.io/github/license/mashape/apistatus.svg)](https://en.wikipedia.org/wiki/MIT_License)
## About
This library simplifies the parsing of command line arguments.

## Features
* Flexible command-line styles, including Linux, Windows and Powershell styles.
* Choose whether to read arguments into an Attribute-decorated class or to read arguments into manually created Parameter objects.
* Support for list parameters
* Support for nullable types

## Code Example

```c#
using Marsonsoft.CommandLineParser;
using System;
using System.Collections.Generic;

namespace SampleCore
{
    public class TokenizeCommandLine
    {
        [Parameter(Name = "version", Description = "Output version information and exit.")]
        public bool Version { get; set; }

        [Parameter(Name = "whitespace", Aliases = new[] { "w" }, Description = "Comma-separated list of characters to treat as whitespace in decimal, example: 8,9,10,11,12,13")]
        public List<int> Whitespace { get; set; }

        [Parameter(Name = "output", Aliases = new[] { "o" }, Description = "Specify output file (instead of stdout).")]
        public string OutputFilePath { get; set; }

        [Parameter(Description = "Input file(s)")]
        public List<string> InputFilePaths { get; set; }
    }

    internal static class ReadIntoClass
    {
        public static void Run(string[] args)
        {
            var parser = new Parser(new ParserSetup());
            var tokenizeCommandLine = parser.Parse<TokenizeCommandLine>(args);
            if (tokenizeCommandLine.Version)
            {
                Console.WriteLine("Tokenize Command Line version 1.2.3");
                return;
            }
            //...
        }
    }
}
```

## Example Command Lines for Code Example
### Linux
 ```bash
dotnet SampleCore.dll --whitespace=8,9,10,11,12,13 --output=out.txt --dateTimeFilter 2018-07-18T10:12:07 file1.txt file2.txt file3.txt
dotnet SampleCore.dll -w 8,9,10,11,12,13 -o out.txt -d 2018-07-18T10:12:07 file1.txt file2.txt file3.txt
```

### Powershell
If the parser is initialized with PowerShell setup:
```c#
var parser = new Parser(ParserSetup.CreatePowershellStyle());
```
...then you can invoke the command like this:
 ```powershell
dotnet SampleCore.dll -whitespace 8,9,10,11,12,13 -output out.txt -dateTimeFilter 2018-07-18T10:12:07 file1.txt file2.txt file3.txt
dotnet SampleCore.dll -w 8,9,10,11,12,13 -o out.txt -d 2018-07-18T10:12:07 file1.txt file2.txt file3.txt
```

### Windows
If the parser is initialized with Windows setup:
```c#
var parser = new Parser(ParserSetup.CreateWindowsStyle());
```
...then you can invoke the command like this:
 ```powershell
dotnet SampleCore.dll /whitespace:8,9,10,11,12,13 /output:out.txt /dateTimeFilter:2018-07-18T10:12:07 file1.txt file2.txt file3.txt
dotnet SampleCore.dll /w:8,9,10,11,12,13 /o:out.txt /d:2018-07-18T10:12:07 file1.txt file2.txt file3.txt
```

## License
The project is licensed under the MIT license.