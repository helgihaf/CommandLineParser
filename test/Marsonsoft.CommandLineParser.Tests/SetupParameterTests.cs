﻿
using Xunit;

namespace Marsonsoft.CommandLineParser.Tests
{
    public class SetupParameterTests
    {
        [Fact]
        public void ShouldCreateStringParam()
        {
            var parameter = new Parameter<string>();
        }

        [Fact]
        public void ShouldNamedParam()
        {
            var parameter = new Parameter<string>
            {
                Name = "myParam",
            };
        }

        [Fact]
        public void ShouldNamedAndDescriptionParam()
        {
            var parameter = new Parameter<string>
            {
                Name = "myParam",
                Description = "my param description",
            };
        }

        [Fact]
        public void ShouldNameAndShortNameAndDescriptionParam()
        {
            var parameter = new Parameter<string>
            {
                Name = "myParam",
                Aliases = new[] { "p" },
                Description = "my param description",
            };
        }

        [Fact]
        public void ShouldNameAndAliasAndDescriptionAndPositionParam()
        {
            var parameter = new Parameter<string>
            {
                Name = "myParam",
                Aliases = new[] { "p" },
                Description = "my param description",
                Position = 0,
            };
        }
    }
}
