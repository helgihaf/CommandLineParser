﻿namespace Marsonsoft.CommandLineParser.Tests
{
    internal static class ParserSetupExtensions
    {
        public static string ShortOrNameParameterToken(this ParserSetup parserSetup)
        {
            return parserSetup.ShortParameterToken ?? parserSetup.ParameterToken;
        }

        //public static void SetParameterShortNameOrName<T>(this ParserSetup parserSetup, Parameter<T> param, string shortName)
        //{
        //    if (parserSetup.ShortParameterToken != null)
        //    {
        //        param.ShortName = shortName;
        //    }
        //    else
        //    {
        //        param.Name = shortName;
        //    }
        //}

        public static string AssignmentTokenOrSpace(this ParserSetup parserSetup)
        {
            return parserSetup.AssignmentToken ?? " ";
        }

        //public static string Adjust(this ParserSetup parserSetup, string arg)
        //{
        //	var sb = new StringBuilder();
        //	foreach (var token in Tokenize(arg))
        //	{
        //		if (IsPropertyValue(token))
        //		{
        //			sb.Append(GetPropertyValue(parserSetup, token));
        //		}
        //		else
        //		{
        //			sb.Append(token);
        //		}
        //	}

        //	return sb.ToString();
        //}

        //private static IEnumerable<string> Tokenize(string arg)
        //{
        //	int index = 0;
        //	var tokens = new List<string>();
        //	var currentToken = new StringBuilder();
        //	while (index < arg.Length)
        //	{
        //		if (arg[index] == '$')
        //		{
        //			index++;
        //			if (currentToken.Length > 0)
        //			{
        //				tokens.Add(currentToken.ToString());
        //				currentToken.Clear();
        //			}
        //			if (arg[index] == '(')
        //			{
        //				index++;
        //				int startIndex = index;
        //				index = arg.IndexOf(')', index);
        //				if (index == -1)
        //				{
        //					throw new Exception("Missing closing parentheses");
        //				}
        //				// 0123456
        //				// $(ad)pq
        //				// startIndex = 2
        //				// index = 4
        //				// length = 4 -2 = 2
        //				tokens.Add("$" + arg.Substring(startIndex, index - startIndex));
        //				index++;
        //			}
        //			else
        //			{
        //				currentToken.Append(arg[index]);
        //				index++;
        //			}
        //		}
        //		else
        //		{
        //			currentToken.Append(arg[index]);
        //			index++;
        //		}
        //	}
        //	if (currentToken.Length > 0)
        //	{
        //		tokens.Add(currentToken.ToString());
        //		currentToken.Clear();
        //	}

        //	return tokens;
        //}

        //private static bool IsPropertyValue(string token)
        //{
        //	return token.StartsWith("$");
        //}

        //private static string GetPropertyValue(ParserSetup parserSetup, string token)
        //{
        //	switch (token)
        //	{
        //		case "$ParameterToken":
        //			return parserSetup.ParameterToken;

        //		case "$ShortParameterToken":
        //			return parserSetup.ShortParameterToken ?? parserSetup.ParameterToken;

        //		case "$AssignmentToken":
        //			return parserSetup.AssignmentToken;

        //		default:
        //			throw new Exception("Property name for " + token + " not found");
        //	}
        //}
    }
}
