﻿using System.Collections.Generic;

namespace Marsonsoft.CommandLineParser.Tests
{
    public class Subject
    {
        [Parameter(Name = "switch", Aliases = new[] { "s" }, Description = "A switch.")]
        public bool Switch { get; set; }

        [Parameter(Name = "number", Aliases = new[] { "n" }, Description = "A number.")]
        public int Number { get; set; }

        [Parameter(Name = "list", Aliases = new[] { "l" }, Description = "A named list.")]
        public List<int> NamedList { get; set; }


        [Parameter(Name = "glist", Aliases = new[] { "g" }, Description = "A named, getter-only list.")]
        public List<string> NamedGetterOnlyList { get; } = new List<string>();

        [Parameter(Name = "emptyList")]
        public List<string> EmptyList { get; } = new List<string>();

        [Parameter(Description = "First positional.")]
        public string FirstPositional { get; set; }

        [Parameter(Description = "A positional list.")]
        public List<string> PositionalList { get; set; }
    }
}
