﻿using System;
using System.Globalization;
using System.Threading;

namespace Marsonsoft.CommandLineParser.Tests
{
    internal class CultureScope : IDisposable
    {
        private CultureInfo previousCultureInfo;

        public CultureScope(CultureInfo cultureInfo)
        {
            this.previousCultureInfo = CultureInfo.CurrentCulture;
            CultureInfo.CurrentCulture = cultureInfo;
        }

        public void Dispose()
        {
            CultureInfo.CurrentCulture = previousCultureInfo;
        }
    }
}