﻿using Xunit;

namespace Marsonsoft.CommandLineParser.Tests
{
    public class AttributeTests
    {
        [Fact]
        public void ShouldParseIntoSubjectClass()
        {
            var parserSetup = new ParserSetup();
            var parser = new Parser(parserSetup);

            string commandLine =
                "-s -n10 --list=1,2,3 --glist get1,get2,get3 firstPositional item1,item2,item3";

            var subject = parser.Parse<Subject>(Splitter.Split(commandLine));

            Assert.Equal(true, subject.Switch);

            Assert.Equal(10, subject.Number);

            Assert.Equal(3, subject.NamedList.Count);
            for (int i = 0; i < 3; i++)
            {
                Assert.Equal(i + 1, subject.NamedList[i]);
            }

            Assert.Equal(3, subject.NamedGetterOnlyList.Count);
            for (int i = 0; i < 3; i++)
            {
                Assert.Equal("get" + (i + 1), subject.NamedGetterOnlyList[i]);
            }

            Assert.Equal("firstPositional", subject.FirstPositional);

            Assert.Equal(3, subject.PositionalList.Count);
            for (int i = 0; i < 3; i++)
            {
                Assert.Equal("item" + (i + 1), subject.PositionalList[i]);
            }
        }

        [Fact]
        public void ShouldParseEmptyListIntoSubjectClass()
        {
            var parserSetup = new ParserSetup();
            var parser = new Parser(parserSetup);

            string commandLine = "--emptyList";

            var subject = parser.Parse<Subject>(Splitter.Split(commandLine));

            Assert.Equal(0, subject.EmptyList.Count);
        }
    }
}
