﻿using System;
using Xunit;

namespace Marsonsoft.CommandLineParser.Tests
{
    public class SetupTests
    {
        [Fact]
        public void ShouldCreateSetupWithDefaultShortParameterToken()
        {
            var parserSetup = new ParserSetup();
            Assert.Equal("-", parserSetup.ShortParameterToken);
        }

        [Fact]
        public void ShouldCreateSetupWithDefaultParameterToken()
        {
            var parserSetup = new ParserSetup();
            Assert.Equal("--", parserSetup.ParameterToken);
        }

        [Fact]
        public void ShouldCreateSetupWithDefaultAssignmentToken()
        {
            var parserSetup = new ParserSetup();
            Assert.Equal("=", parserSetup.AssignmentToken);
        }

        [Fact]
        public void ShouldCreateSetupWithDefaultStringComparison()
        {
            var parserSetup = new ParserSetup();
            Assert.Equal(StringComparison.CurrentCulture, parserSetup.StringComparison);
        }

        [Fact]
        public void ShouldCreateSetupWithLinuxStyle()
        {
            var parserSetup = ParserSetup.CreateLinuxStyle();
            Assert.Equal("-", parserSetup.ShortParameterToken);
            Assert.Equal("--", parserSetup.ParameterToken);
            Assert.Equal("=", parserSetup.AssignmentToken);
            Assert.Equal(StringComparison.CurrentCulture, parserSetup.StringComparison);
        }

        [Fact]
        public void ShouldCreateSetupWithWindowsStyle()
        {
            var parserSetup = ParserSetup.CreateWindowsStyle();
            Assert.Equal(null, parserSetup.ShortParameterToken);
            Assert.Equal("/", parserSetup.ParameterToken);
            Assert.Equal(":", parserSetup.AssignmentToken);
            Assert.Equal(StringComparison.CurrentCultureIgnoreCase, parserSetup.StringComparison);
        }

        [Fact]
        public void ShouldCreateSetupWithPowershellStyle()
        {
            var parserSetup = ParserSetup.CreatePowershellStyle();
            Assert.Equal(null, parserSetup.ShortParameterToken);
            Assert.Equal("-", parserSetup.ParameterToken);
            Assert.Equal(null, parserSetup.AssignmentToken);
            Assert.Equal(StringComparison.CurrentCultureIgnoreCase, parserSetup.StringComparison);
        }
    }
}
