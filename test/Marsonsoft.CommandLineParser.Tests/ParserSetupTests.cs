﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Marsonsoft.CommandLineParser.Tests
{
    public class ParserSetupTests
    {
        [Fact]
        public void ShouldThrowExceptionWhenEmptyParamToken()
        {
            var parserSetup = new ParserSetup
            {
                ParameterToken = string.Empty,
            };
            Assert.Throws<ArgumentException>(() => new Parser(parserSetup));
        }

        [Fact]
        public void ShouldThrowExceptionWhenEmptyShortParamToken()
        {
            var parserSetup = new ParserSetup
            {
                ShortParameterToken = string.Empty,
            };
            Assert.Throws<ArgumentException>(() => new Parser(parserSetup));
        }

        [Fact]
        public void ShouldThrowExceptionWhenShortAndNormalParamTokensAreEqual()
        {
            var parserSetup = new ParserSetup
            {
                ParameterToken = "-",
                ShortParameterToken = "-",
                AssignmentToken = null,
                StringComparison = StringComparison.CurrentCultureIgnoreCase,
            };
            Assert.Throws<ArgumentException>(() => new Parser(parserSetup));
        }

        [Fact]
        public void ShouldThrowExceptionWhenEmptyAssignmentToken()
        {
            var parserSetup = new ParserSetup
            {
                AssignmentToken = string.Empty,
            };
            Assert.Throws<ArgumentException>(() => new Parser(parserSetup));
        }
    }
}
