﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Xunit;

namespace Marsonsoft.CommandLineParser.Tests
{
    public abstract class UberTestsBase
    {
        protected static void AssertListAreEqual(string[] values, IList<string> list)
        {
            Assert.Equal(values.Length, list.Count);
            for (int i = 0; i < values.Length; i++)
            {
                Assert.Equal(values[i], list[i]);
            }
        }

        protected abstract ParserSetup CreateParserSetup();

        protected void CreateParser()
        {
            var parser = new Parser(CreateParserSetup());
        }

        protected void ParseStringParameterPositional()
        {
            var parserSetup = CreateParserSetup();

            var parameter = new Parameter<string>();
            var parser = new Parser(parserSetup);

            const string TestValue = "myValue";
            parser.Parse(new[] { parameter }, new[] { TestValue });
            Assert.Equal(TestValue, parameter.Argument.Value);
            Assert.Equal(TestValue, parameter.Argument.RawValue);
            Assert.Equal(TestValue, parameter.Argument.ValueObject);
        }

        protected void ParseStringParameterWithListSeperator()
        {
            var parserSetup = CreateParserSetup();

            var parameter = new Parameter<string>();
            var parser = new Parser(parserSetup);

            const string TestValue = "myValue,isFun";
            parser.Parse(new[] { parameter }, new[] { TestValue });
            Assert.Equal(TestValue, parameter.Argument.Value);
            Assert.Equal(TestValue, parameter.Argument.RawValue);
            Assert.Equal(TestValue, parameter.Argument.ValueObject);
        }

        protected void ParseDateTimeParameterPositional()
        {
            ParserSetup parserSetup = CreateParserSetup();
            var parameter = new Parameter<DateTime>();
            var parser = new Parser(parserSetup);

            using (new CultureScope(CultureInfo.InvariantCulture))
            {
                DateTime testDateTimeValue = DateTime.Now;
                string testStringValue = testDateTimeValue.ToString();
                ParseDateTime(parser, parameter, testDateTimeValue, testStringValue);
            }
        }

        protected void ParseDateTimeParameterPositionalSortableFormat()
        {
            ParserSetup parserSetup = CreateParserSetup();
            var parameter = new Parameter<DateTime>();
            var parser = new Parser(parserSetup);

            DateTime testDateTimeValue = DateTime.Now;
            string testStringValue = testDateTimeValue.ToString("s");
            ParseDateTime(parser, parameter, testDateTimeValue, testStringValue);
        }

        protected void ParseDateTimeNullableParameterPositional()
        {
            ParserSetup parserSetup = CreateParserSetup();
            var parameter = new Parameter<DateTime?>();
            var parser = new Parser(parserSetup);

            DateTime testDateTimeValue = DateTime.Now;
            string testStringValue = testDateTimeValue.ToString("s");
            parser.Parse(new[] { parameter }, new[] { testStringValue });
            Assert.True(testDateTimeValue.AddSeconds(-1) < parameter.Argument.Value.Value && parameter.Argument.Value.Value < testDateTimeValue.AddSeconds(1));
            Assert.Equal(parameter.Argument.Value.Value, parameter.Argument.ValueObject);
            Assert.Equal(testStringValue, parameter.Argument.RawValue);
        }

        private void ParseDateTime(Parser parser, Parameter<DateTime> parameter, DateTime testDateTimeValue, string testStringValue)
        {
            parser.Parse(new[] { parameter }, new[] { testStringValue });
            Assert.True(testDateTimeValue.AddSeconds(-1) < parameter.Argument.Value && parameter.Argument.Value < testDateTimeValue.AddSeconds(1));
            Assert.Equal(parameter.Argument.Value, parameter.Argument.ValueObject);
            Assert.Equal(testStringValue, parameter.Argument.RawValue);
        }

        protected void ParseShortNamedParameterBool()
        {
            const string shortName = "p";

            ParserSetup parserSetup = CreateParserSetup();
            string arg = $"{parserSetup.ShortOrNameParameterToken()}p";

            var privilegedParam = new Parameter<bool>();
            privilegedParam.Name = shortName;
            var parser = new Parser(parserSetup);


            var args = new[] { arg };
            parser.Parse(new[] { privilegedParam }, args);
            Assert.Equal(true, privilegedParam.Argument.Value);
            Assert.Equal(true, privilegedParam.Argument.ValueObject);
            Assert.Equal(args[0], privilegedParam.Argument.RawValue);
        }

        protected void ParseNamedParameterBool()
        {
            const string name = "privileged";

            ParserSetup parserSetup = CreateParserSetup();
            string arg = $"{parserSetup.ParameterToken}privileged";

            var privilegedParam = new Parameter<bool>
            {
                Name = name
            };
            var parser = new Parser(parserSetup);

            var args = new[] { arg };
            parser.Parse(new[] { privilegedParam }, args);
            Assert.Equal(true, privilegedParam.Argument.Value);
            Assert.Equal(true, privilegedParam.Argument.ValueObject);
            Assert.Equal(args[0], privilegedParam.Argument.RawValue);
        }

        protected void ParseShortNamedParameterWithNospaceValue()
        {
            var parserSetup = CreateParserSetup();
            Assert.NotNull(parserSetup.ShortParameterToken); // Do not use this test unless setup has a ShortParameterToken

            var privilegedParam = new Parameter<string>
            {
                Name = "p"
            };
            var parser = new Parser(parserSetup);

            var args = new[] { $"{parserSetup.ShortParameterToken}pthevalue" };
            parser.Parse(new[] { privilegedParam }, args);
            Assert.Equal("thevalue", privilegedParam.Argument.Value);
            Assert.Equal("thevalue", privilegedParam.Argument.ValueObject);
            Assert.Equal(args[0], privilegedParam.Argument.RawValue);
        }

        protected void ParseShortNamedParameterWithSpaceValue()
        {
            var parserSetup = CreateParserSetup();
            var privilegedParam = new Parameter<string>();
            privilegedParam.Name = "p";
            var parser = new Parser(parserSetup);


            var args = new[] { $"{parserSetup.ShortOrNameParameterToken()}p", "thevalue" };
            parser.Parse(new[] { privilegedParam }, args);
            Assert.Equal("thevalue", privilegedParam.Argument.Value);
            Assert.Equal("thevalue", privilegedParam.Argument.ValueObject);
            Assert.Equal(args[0] + " " + args[1], privilegedParam.Argument.RawValue);
        }

        protected void ParseShortNamedParameterWithSpaceValueFollowedByNormalValue()
        {
            var parserSetup = CreateParserSetup();
            Assert.NotNull(parserSetup.ShortParameterToken); // Do not use this test unless setup has a ShortParameterToken

            var privilegedParam = new Parameter<string>
            {
                Name = "p"
            };

            var positionalParam = new Parameter<string>();

            var parser = new Parser(parserSetup);

            var args = new[] { $"{parserSetup.ShortParameterToken}pthevalue", "normalValue" };
            parser.Parse(new[] { privilegedParam, positionalParam }, args);

            Assert.Equal("thevalue", privilegedParam.Argument.Value);
            Assert.Equal("thevalue", privilegedParam.Argument.ValueObject);
            Assert.Equal(args[0], privilegedParam.Argument.RawValue);

            Assert.Equal("normalValue", positionalParam.Argument.Value);
            Assert.Equal("normalValue", positionalParam.Argument.ValueObject);
            Assert.Equal(args[1], positionalParam.Argument.RawValue);
        }

        protected void ParseSingleLongOptionWithValue()
        {
            const string privilegedValue = "extremely";

            var parserSetup = CreateParserSetup();
            string commandLine = $"{parserSetup.ParameterToken}privileged{parserSetup.AssignmentTokenOrSpace()}{privilegedValue}";

            var privilegedParam = new Parameter<string>
            {
                Name = "privileged"
            };
            var parser = new Parser(parserSetup);

            var args = Splitter.Split(commandLine);
            parser.Parse(new[] { privilegedParam }, args);

            Assert.Equal(privilegedValue, privilegedParam.Argument.Value);
            Assert.Equal(privilegedValue, privilegedParam.Argument.ValueObject);
            Assert.Equal(commandLine, privilegedParam.Argument.RawValue);
        }

        protected void ParseSingleLongOptionWithMultipleValues()
        {
            var parserSetup = CreateParserSetup();
            var commandLine = string.Format("{0}privileged{1}extremely{2}somewhat{2}rarely",
                parserSetup.ParameterToken,
                parserSetup.AssignmentTokenOrSpace(),
                parserSetup.ListSeparators[0]);
            var values = new[] { "extremely", "somewhat", "rarely" };

            var theParam = new ListParameter<string>
            {
                Name = "privileged"
            };
            var parser = new Parser(parserSetup);
            var args = Splitter.Split(commandLine);
            parser.Parse(new[] { theParam }, args);

            for (int i = 0; i < theParam.Argument.Value.Count; i++)
            {
                Assert.Equal(values[i], theParam.Argument.Value[i]);
            }
            Assert.Equal(theParam.Argument.Value, theParam.Argument.ValueObject);
            Assert.Equal(commandLine, theParam.Argument.RawValue);
        }

        protected void ParseSingleLongListOptionWithSingleValue()
        {
            var parserSetup = CreateParserSetup();
            var commandLine = string.Format("{0}privileged{1}extremely",
                parserSetup.ParameterToken,
                parserSetup.AssignmentTokenOrSpace());
            var values = new[] { "extremely" };

            var theParam = new ListParameter<string>
            {
                Name = "privileged"
            };
            var parser = new Parser(parserSetup);
            var args = Splitter.Split(commandLine);
            parser.Parse(new[] { theParam }, args);

            for (int i = 0; i < theParam.Argument.Value.Count; i++)
            {
                Assert.Equal(values[i], theParam.Argument.Value[i]);
            }
            Assert.Equal(theParam.Argument.Value, theParam.Argument.ValueObject);
            Assert.Equal(commandLine, theParam.Argument.RawValue);
        }

        protected void ParseMultipleValues()
        {
            var parserSetup = CreateParserSetup();
            var param1 = new Parameter<string>();
            var param2 = new Parameter<string>();
            var parser = new Parser(parserSetup);
            var args = new[] { "hello", "world" };
            parser.Parse(new[] { param1, param2 }, args);

            Assert.Equal("hello", param1.Argument.Value);
            Assert.Equal("world", param2.Argument.Value);
        }

        protected void ParseMultipleValuesAsListPositional()
        {
            var parserSetup = CreateParserSetup();
            var listParam = new ListParameter<string>();
            var parser = new Parser(parserSetup);
            var args = new[] { "hello", "world" };
            parser.Parse(new[] { listParam }, args);

            Assert.Equal(2, listParam.Argument.Value.Count);
            Assert.Equal("hello", listParam.Argument.Value[0]);
            Assert.Equal("world", listParam.Argument.Value[1]);
        }

        protected void ParseMultipleSwitches()
        {
            var parserSetup = CreateParserSetup();
            string[] names = new[] { "p", "h" };
            string[] args = new[] { parserSetup.ShortOrNameParameterToken() + "p", parserSetup.ShortOrNameParameterToken() + "h" };
            Assert.Equal(names.Length, args.Length);
            var parameters = new Parameter<bool>[names.Length];
            for (int i = 0; i < parameters.Length; i++)
            {
                parameters[i] = new Parameter<bool>();
                parameters[i].Name = names[i];
            }
            var parser = new Parser(parserSetup);
            parser.Parse(parameters, args);

            for (int i = 0; i < parameters.Length; i++)
            {
                Assert.Equal(true, parameters[i].Argument.Value);
            }
        }

        protected void ParseTwoNamedParametersWithValues()
        {
            const string privilegedValue = "extremely";
            const string includeValue = "foo.h";

            var parserSetup = CreateParserSetup();

            var names = new[] { "privileged", "include" };
            var values = new[] { privilegedValue, includeValue };
            var commandLine =
                $"{parserSetup.ParameterToken}privileged{parserSetup.AssignmentTokenOrSpace()}{privilegedValue}" +
                " " +
                $"{parserSetup.ParameterToken}include{parserSetup.AssignmentTokenOrSpace()}{includeValue}";

            Assert.Equal(names.Length, values.Length);

            var parameters = new Parameter<string>[names.Length];
            for (int i = 0; i < parameters.Length; i++)
            {
                parameters[i] = new Parameter<string>() { Name = names[i] };
            }
            var parser = new Parser(parserSetup);

            var args = Splitter.Split(commandLine);
            parser.Parse(parameters, args);

            for (int i = 0; i < parameters.Length; i++)
            {
                Assert.Equal(values[i], parameters[i].Argument.Value);
                Assert.Equal(values[i], parameters[i].Argument.ValueObject);
            }
        }

        protected void ParseNullArguments()
        {
            var parserSetup = CreateParserSetup();
            var parser = new Parser(parserSetup);
            parser.Parse(new[] { new Parameter<string>() }, null);
        }

        protected void ParseEmptyArguments()
        {
            var parserSetup = CreateParserSetup();
            var parser = new Parser(parserSetup);
            parser.Parse(new[] { new Parameter<string>() }, new string[0]);
        }

        protected void ParseEmptyString()
        {
            var parserSetup = CreateParserSetup();
            var parser = new Parser(parserSetup);
            parser.Parse(new[] { new Parameter<string>() }, new[] { "" });
        }

        protected void ParseWhiteSpace()
        {
            var parserSetup = CreateParserSetup();
            var parser = new Parser(parserSetup);
            parser.Parse(new[] { new Parameter<string>() }, new[] { "   \t\t\t \r\n  " });
        }

        protected void ParseParameterWithEmptyValue()
        {
            var parserSetup = CreateParserSetup();
            var privilegedParam = new Parameter<string>
            {
                Name = "privileged"
            };
            var parser = new Parser(parserSetup);

            var args = new[] { $"{parserSetup.ParameterToken}privileged{parserSetup.AssignmentTokenOrSpace()}".Trim() };
            parser.Parse(new[] { privilegedParam }, args);

            Assert.Equal("", privilegedParam.Argument.Value);
        }

        protected void ParseParameterWithEmptyValueFollowedByAnotherParameter()
        {
            var parserSetup = CreateParserSetup();
            var privilegedParam = new Parameter<string>
            {
                Name = "privileged"
            };
            var includeParam = new Parameter<string>
            {
                Name = "include"
            };
            var parser = new Parser(parserSetup);

            var commandLine = 
                $"{parserSetup.ParameterToken}privileged{parserSetup.AssignmentTokenOrSpace()}".Trim() +
                " " +
                $"{parserSetup.ParameterToken}include{parserSetup.AssignmentTokenOrSpace()}foo.c";
            parser.Parse(new[] { privilegedParam, includeParam }, Splitter.Split(commandLine));

            Assert.Equal("", privilegedParam.Argument.Value);
            Assert.Equal("foo.c", includeParam.Argument.Value);
        }

        protected void SupportCustomListSeperators()
        {
            var parserSetup = CreateParserSetup();
            parserSetup.ListSeparators.Add("#");
            var privilegedParam = new ListParameter<string>
            {
                Name = "privileged"
            };
            var parser = new Parser(parserSetup);
            var commandLine = $"{parserSetup.ParameterToken}privileged{parserSetup.AssignmentTokenOrSpace()}extremely#somewhat#rarely";
            parser.Parse(new[] { privilegedParam }, Splitter.Split(commandLine));

            Assert.Equal("extremely", privilegedParam.Argument.Value[0]);
            Assert.Equal("somewhat", privilegedParam.Argument.Value[1]);
        }

        protected void SupportMultipleListSeperators()
        {
            var parserSetup = CreateParserSetup();
            parserSetup.ListSeparators.Add(",");
            parserSetup.ListSeparators.Add(";");
            var privilegedParam = new ListParameter<string>
            {
                Name = "privileged"
            };
            var parser = new Parser(parserSetup);
            var commandLine = $"{parserSetup.ParameterToken}privileged{parserSetup.AssignmentTokenOrSpace()}extremely,somewhat;rarely";
            parser.Parse(new[] { privilegedParam }, Splitter.Split(commandLine));

            Assert.Equal("extremely", privilegedParam.Argument.Value[0]);
            Assert.Equal("somewhat", privilegedParam.Argument.Value[1]);
            Assert.Equal("rarely", privilegedParam.Argument.Value[2]);
        }

        protected void ProcessOtherStyleAsValues(ParserSetup otherStyleSetup)
        {
            // Here we are testing that if you use type-X arguments on type-Y setup that the arguments will be processed as
            // simple values in the type-Y setup. For example, if you use Windows-style arguments while expecting Linux style arguments.

            Assert.NotNull(otherStyleSetup.AssignmentToken); // Do not use this test with null AssignmentToken in {nameof(otherStyleSetup)}.

            var parserSetup = CreateParserSetup();
            var parameters = new IParameter[]
            {
                new Parameter<string>(),
                new ListParameter<string>(),
                new Parameter<string>()
            };

            var parser = new Parser(parserSetup);
            string[] args = GetOtherStyleArgs(otherStyleSetup);

            parser.Parse(parameters, args);

            Assert.Equal(args[0], ((Parameter<string>)parameters[0]).Argument.Value);
            var list = new List<string>();
            list.AddRange(args[1].Split(','));
            list.AddRange(args[2].Split(','));
            list.Add(args[3]);
            AssertListAreEqual(list.ToArray(), ((ListParameter<string>)parameters[1]).Argument.Value);
            Assert.Equal(null, ((Parameter<string>)parameters[2]).Argument);
        }

        protected static string[] GetOtherStyleArgs(ParserSetup otherStyleSetup)
        {
            return new[]
            {
                $"{otherStyleSetup.ParameterToken}p",
                $"{otherStyleSetup.ParameterToken}privileged{otherStyleSetup.AssignmentToken}extremely,somewhat,rarely",
                $"{otherStyleSetup.ParameterToken}include{otherStyleSetup.AssignmentToken}foo.h,bar.h",
                $"{otherStyleSetup.ParameterToken}a{otherStyleSetup.AssignmentToken}thevalue"
            };
        }

        protected void SupportShortNameStringComparison()
        {
            var parserSetup = CreateParserSetup();
            var param1 = new Parameter<string>();
            param1.Name = "p";
            var parser = new Parser(parserSetup);
            bool expectException =
                !string.Equals("p", "P", parserSetup.StringComparison);

            ExpectOKOrException<ArgumentException>(expectException, 
                () => parser.Parse(new[] { param1 }, new[] { $"{parserSetup.ShortOrNameParameterToken()}P" }));
        }

        protected void SupportNameStringComparison()
        {
            var parserSetup = CreateParserSetup();
            var param1 = new Parameter<string>();
            param1.Name = "joeParameter";
            var parser = new Parser(parserSetup);
            bool expectException =
                !string.Equals("p", "P", parserSetup.StringComparison);
            var commandLine = $"{parserSetup.ParameterToken}JOEPARAMETER{parserSetup.AssignmentTokenOrSpace()}thevalue";
            ExpectOKOrException<ArgumentException>(expectException,
                () => parser.Parse(new[] { param1 }, Splitter.Split(commandLine)));
        }

        protected void ThrowArgumentExceptionOnShortNameNotFound()
        {
            var parserSetup = CreateParserSetup();
            var parser = new Parser(parserSetup);
            parser.Parse(new[] { new Parameter<string>() }, new[] { $"{parserSetup.ShortOrNameParameterToken()}P" });
        }

        protected void ThrowArgumentExceptionOnNameNotFound()
        {
            var parserSetup = CreateParserSetup();
            var parser = new Parser(parserSetup);
            var commandLine = $"{parserSetup.ParameterToken}param{parserSetup.AssignmentTokenOrSpace()}thevalue";
            parser.Parse(new[] { new Parameter<string>() }, Splitter.Split(commandLine));
        }

        protected void ThrowArgumentExceptionOnPositionalNotFound()
        {
            var parserSetup = CreateParserSetup();
            var parser = new Parser(parserSetup);
            parser.Parse(new[] { new Parameter<string> { Name = "gaddur" } }, new[] { "thevalue" });
        }

        private static void ExpectOKOrException<T>(bool expectException, Action action) where T : Exception
        {
            Exception exceptionCaught = null;
            try
            {
                action();
            }
            catch (Exception ex)
            {
                exceptionCaught = ex;
            }

            if (expectException)
            {
                if (exceptionCaught == null)
                {
                    Assert.False(true, "Excpected exception but none was thrown.");
                }
                else if ((exceptionCaught as T) == null)
                {
                    Assert.False(true, $"Expected {nameof(T)} but {exceptionCaught.GetType().FullName} was thrown: {exceptionCaught.ToString()}");
                }
            }
            else if (exceptionCaught != null)
            {
                Assert.False(true, $"Excpected no exception but {exceptionCaught.GetType().FullName} was thrown: {exceptionCaught.ToString()}");
            }
        }
    }
}