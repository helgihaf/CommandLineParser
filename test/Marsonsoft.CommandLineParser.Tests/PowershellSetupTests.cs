﻿using System;
using System.Collections.Generic;
using Xunit;

namespace Marsonsoft.CommandLineParser.Tests
{
    public class PowershellSetupTests : UberTestsBase
    {
        protected override ParserSetup CreateParserSetup()
        {
            return ParserSetup.CreatePowershellStyle();
        }

        [Fact]
        public void ShouldCreateParser()
        {
            CreateParser();
        }

        [Fact]
        public void ShouldParseStringParameterPositional()
        {
            ParseStringParameterPositional();
        }

        [Fact]
        public void ShouldParseDateTimeParameterPositional()
        {
            ParseDateTimeParameterPositional();
        }

        [Fact]
        public void ShouldParseDateTimeParameterPositionalSortableFormat()
        {
            ParseDateTimeParameterPositionalSortableFormat();
        }

        [Fact]
        public void ShouldParseDateTimeNullableParameterPositional()
        {
            ParseDateTimeNullableParameterPositional();
        }

        [Fact]
        public void ShouldParseShortNamedParameterBool()
        {
            ParseShortNamedParameterBool();
        }

        [Fact]
        public void ShouldParseNamedParameterBool()
        {
            ParseNamedParameterBool();
        }

        [Fact]
        public void ShouldParseShortNamedParameterWithSpaceValue()
        {
            ParseShortNamedParameterWithSpaceValue();
        }

        [Fact]
        public void ShouldParseSingleLongOptionWithValue()
        {
            ParseSingleLongOptionWithValue();
        }

        [Fact]
        public void ShouldParseSingleLongOptionWithMultipleValues()
        {
            ParseSingleLongOptionWithMultipleValues();
        }

        [Fact]
        public void ShouldParseSingleLongListOptionWithSingleValue()
        {
            ParseSingleLongListOptionWithSingleValue();
        }

        [Fact]
        public void ShouldParseMultipleValues()
        {
            ParseMultipleValues();
        }

        [Fact]
        public void ShouldParseMultipleShortNames()
        {
            ParseMultipleSwitches();
        }

        [Fact]
        public void ShouldParseTwoNamedParametersWithValues()
        {
            ParseTwoNamedParametersWithValues();
        }

        //--
        [Fact]
        public void ShouldParseNullArguments()
        {
            ParseNullArguments();
        }

        [Fact]
        public void ShouldParseEmptyArguments()
        {
            ParseEmptyArguments();
        }

        [Fact]
        public void ShouldParseEmptyString()
        {
            ParseEmptyString();
        }

        [Fact]
        public void ShouldParseWhiteSpace()
        {
            ParseWhiteSpace();
        }

        [Fact]
        public void ShouldParseParameterWithEmptyValue()
        {
            ParseParameterWithEmptyValue();
        }

        [Fact]
        public void ShouldParseParameterWithEmptyValueFollowedByAnotherParameter()
        {
            ParseParameterWithEmptyValueFollowedByAnotherParameter();
        }

        [Fact]
        public void ShouldSupportCustomListSeperators()
        {
            SupportCustomListSeperators();
        }

        [Fact]
        public void ShouldSupportMultipleListSeperators()
        {
            SupportMultipleListSeperators();
        }

        [Fact]
        public void ShouldProcessWindowsStyleAsValues()
        {
            //!!!!ProcessOtherStyleAsValues(ParserSetup.CreateWindowsStyle());
        }

        [Fact]
        public void ShouldSupportShortNameStringComparison()
        {
            SupportShortNameStringComparison();
        }

        [Fact]
        public void ShouldSupportNameStringComparison()
        {
            SupportNameStringComparison();
        }

        [Fact]
        public void ShouldThrowArgumentExceptionOnShortNameNotFound()
        {
            Assert.Throws<ArgumentException>(() => ThrowArgumentExceptionOnShortNameNotFound());
        }

        [Fact]
        public void ShouldThrowArgumentExceptionOnNameNotFound()
        {
            Assert.Throws<ArgumentException>(() => ThrowArgumentExceptionOnNameNotFound());
        }


        [Fact]
        public void ShouldThrowArgumentExceptionOnPositionalNotFound()
        {
            Assert.Throws<ArgumentException>(() => ThrowArgumentExceptionOnPositionalNotFound());
        }

        public class ListValues
        {
            [Parameter(Name = "list", Aliases = new[] { "L" })]
            public List<int> Whitespace { get; set; }
        }

        [Theory]
        [InlineData("-list")]
        [InlineData("-L")]
        public void ShouldIgnoreMissingListValues(string arg)
        {
            var parserSetup = CreateParserSetup();

            var parser = new Parser(parserSetup);
            var args = new[] { arg };
            var a = parser.Parse<ListValues>(args);
        }
    }
}
