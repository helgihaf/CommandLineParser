﻿using System;
using Xunit;

namespace Marsonsoft.CommandLineParser.Tests
{
    public class ParserDefaultSetupUberTests : UberTestsBase
    {
        protected override ParserSetup CreateParserSetup()
        {
            return new ParserSetup();
        }

        [Fact]
        public void ShouldCreateParser()
        {
            CreateParser();
        }

        [Fact]
        public void ShouldParseStringParameterPositional()
        {
            ParseStringParameterPositional();
        }

        [Fact]
        public void ShouldParseStringParameterWithListSeperator()
        {
            ParseStringParameterWithListSeperator();
        }

        [Fact]
        public void ShouldParseDateTimeParameterPositional()
        {
            ParseDateTimeParameterPositional();
        }

        [Fact]
        public void ShouldParseDateTimeParameterPositionalSortableFormat()
        {
            ParseDateTimeParameterPositionalSortableFormat();
        }

        [Fact]
        public void ShouldParseDateTimeNullableParameterPositional()
        {
            ParseDateTimeNullableParameterPositional();
        }

        [Fact]
        public void ShouldParseShortNamedParameterBool()
        {
            ParseShortNamedParameterBool();
        }

        [Fact]
        public void ShouldParseNamedParameterBool()
        {
            ParseNamedParameterBool();
        }

        [Fact]
        public void ShouldParseShortNamedParameterWithNospaceValue()
        {
            ParseShortNamedParameterWithNospaceValue();
        }

        [Fact]
        public void ShouldParseShortNamedParameterWithSpaceValue()
        {
            ParseShortNamedParameterWithSpaceValue();
        }

        [Fact]
        public void ShouldParseShortNamedParameterWithSpaceValueFollowedByNormalValue()
        {
            ParseShortNamedParameterWithSpaceValueFollowedByNormalValue();
        }

        [Fact]
        public void ShouldParseSingleLongOptionWithValue()
        {
            ParseSingleLongOptionWithValue();
        }

        [Fact]
        public void ShouldParseSingleLongOptionWithMultipleValues()
        {
            ParseSingleLongOptionWithMultipleValues();
        }

        [Fact]
        public void ShouldParseSingleLongListOptionWithSingleValue()
        {
            ParseSingleLongListOptionWithSingleValue();
        }

        [Fact]
        public void ShouldParseMultipleValues()
        {
            ParseMultipleValues();
        }

        [Fact]
        public void ShouldParseMultipleValuesAsListPositional()
        {
            ParseMultipleValuesAsListPositional();
        }

        [Fact]
        public void ShouldParseMultipleShortNames()
        {
            ParseMultipleSwitches();
        }

        [Fact]
        public void ShouldParseTwoNamedParametersWithValues()
        {
            ParseTwoNamedParametersWithValues();
        }

        //!!common
        [Fact]
        public void ShouldThrowArgumentNullExceptionOnNullSetup()
        {
            Assert.Throws<ArgumentNullException>(() => new Parser(null));
        }

        //--
        [Fact]
        public void ShouldParseNullArguments()
        {
            ParseNullArguments();
        }

        [Fact]
        public void ShouldParseEmptyArguments()
        {
            ParseEmptyArguments();
        }

        [Fact]
        public void ShouldParseEmptyString()
        {
            ParseEmptyString();
        }

        [Fact]
        public void ShouldParseWhiteSpace()
        {
            ParseWhiteSpace();
        }

        [Fact]
        public void ShouldParseParameterWithEmptyValue()
        {
            ParseParameterWithEmptyValue();
        }

        [Fact]
        public void ShouldParseParameterWithEmptyValueFollowedByAnotherParameter()
        {
            ParseParameterWithEmptyValueFollowedByAnotherParameter();
        }

        [Fact]
        public void ShouldSupportCustomListSeperators()
        {
            SupportCustomListSeperators();
        }

        [Fact]
        public void ShouldSupportMultipleListSeperators()
        {
            SupportMultipleListSeperators();
        }

        [Fact]
        public void ShouldProcessWindowsStyleAsValues()
        {
            ProcessOtherStyleAsValues(ParserSetup.CreateWindowsStyle());
        }

        [Fact]
        public void ShouldSupportShortNameStringComparison()
        {
            SupportShortNameStringComparison();
        }

        [Fact]
        public void ShouldSupportNameStringComparison()
        {
            SupportNameStringComparison();
        }


        [Fact]
        public void ShouldThrowArgumentExceptionOnShortNameNotFound()
        {
            Assert.Throws<ArgumentException>(() => ThrowArgumentExceptionOnShortNameNotFound());
        }

        [Fact]
        public void ShouldThrowArgumentExceptionOnNameNotFound()
        {
            Assert.Throws<ArgumentException>(() => ThrowArgumentExceptionOnNameNotFound());
        }


        [Fact]
        public void ShouldThrowArgumentExceptionOnPositionalNotFound()
        {
            Assert.Throws<ArgumentException>(() => ThrowArgumentExceptionOnPositionalNotFound());
        }

        [Fact]
        public void ShouldParseTwoFlagsFromOneShortParamToken()
        {
            var parserSetup = CreateParserSetup();

            var flag1Parameter = new Parameter<bool> { Name = "i" };
            var flag2Parameter = new Parameter<bool> { Name = "t" };
            var parser = new Parser(parserSetup);
            string arg = $"-it";

            var args = new[] { arg };
            parser.Parse(new[] { flag1Parameter, flag2Parameter }, args);
            Assert.Equal(true, flag1Parameter.Argument.Value);
            Assert.Equal(true, flag1Parameter.Argument.ValueObject);
            Assert.Equal("-it", flag1Parameter.Argument.RawValue);
            Assert.Equal(true, flag2Parameter.Argument.Value);
            Assert.Equal(true, flag2Parameter.Argument.ValueObject);
            Assert.Equal("-it", flag2Parameter.Argument.RawValue);
        }

        [Theory]
        [InlineData("--list")]
        [InlineData("--list=")]
        [InlineData("-L")]
        [InlineData("-L=")]
        public void ShouldIgnoreMissingListValues(string arg)
        {
            var parserSetup = CreateParserSetup();

            var listParameter = new ListParameter<string> { Name = "list", Aliases = new[] { "L" } };
            var parser = new Parser(parserSetup);
            var args = new[] { arg };
            parser.Parse(new[] { listParameter }, args);
        }
    }
}
