﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Marsonsoft.CommandLineParser.Tests
{
    public class CustomSetupTests
    {
        [Fact]
        public void ShouldParsePositionalWhenNullParameterToken()
        {
            var parserSetup = new ParserSetup
            {
                ParameterToken = null,
                ShortParameterToken = null,
                AssignmentToken = null,
                StringComparison = StringComparison.CurrentCultureIgnoreCase,
            };
            var param1 = new Parameter<string>();
            var parser = new Parser(parserSetup);
            var args = new[] { "hello" };
            parser.Parse(new[] { param1 }, args);

            Assert.Equal("hello", param1.Argument.Value);
        }
    }
}
