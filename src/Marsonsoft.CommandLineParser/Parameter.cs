﻿namespace Marsonsoft.CommandLineParser
{
    /// <summary>
    /// Represents a command-line parameter for non-list valued arguments.
    /// </summary>
    /// <typeparam name="T">The value type of the parameter.</typeparam>
    /// <seealso cref="Marsonsoft.CommandLineParser.ParameterBase{T}" />
    public class Parameter<T> : ParameterBase<T>
    {
        /// <summary>
        /// Gets the argument, if any was found in the last parsing.
        /// </summary>
        public Argument<T> Argument { get; private set; }

        /// <summary>
        /// Gets or sets the basic argument, if any was found in the last parsing.
        /// </summary>
        public override IArgument BasicArgument
        {
            get => Argument;
            set => Argument = (Argument<T>)value;
        }

        /// <summary>
        /// Creates an argument based on the specified parameters.
        /// </summary>
        /// <param name="rawValue">The raw value of the argument.</param>
        /// <param name="value">The value of the argument.</param>
        /// <returns>
        /// An instance of <see cref="T:Marsonsoft.CommandLineParser.IArgument" />.
        /// </returns>
        public override IArgument AttachArgument(string rawValue, object value)
        {
            T nativeValue;
            if (value is System.Collections.IEnumerable enumerable && !(value is string))
            {
                nativeValue = GetNativeValue(rawValue);
            }
            else
            {
                nativeValue = GetNativeValue(value);
            }
            Argument = new Argument<T>(this, rawValue, nativeValue);
            return Argument;
        }
    }
}
