﻿using System;

namespace Marsonsoft.CommandLineParser
{
    /// <summary>
    /// Defines the metadata attribute that marks a class property as a command-line parameter.
    /// </summary>
    /// <seealso cref="System.Attribute" />
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class ParameterAttribute : Attribute
    {
        /// <summary>
        /// Gets or sets the unique name of the command-line parameter. Optional.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the optional aliases of the parameter.
        /// </summary>
        public string[] Aliases { get; set; }

        /// <summary>
        /// Gets or sets the description of the command-line parameter. Optional.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the position of the command-line parameter. Optional and only relevant for positional parameters.
        /// </summary>
        public int Position { get; set; } = -1;
    }
}
