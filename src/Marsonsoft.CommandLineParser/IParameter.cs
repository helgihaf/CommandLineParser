﻿using System;
using System.Collections.Generic;

namespace Marsonsoft.CommandLineParser
{
    /// <summary>
    /// Defines a parameter whose values can be read from a command-line.
    /// </summary>
    public interface IParameter
    {
        /// <summary>
        /// Gets the unique name of the parameter.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets the optional aliases of the parameter.
        /// </summary>
        IEnumerable<string> Aliases { get; }

        /// <summary>
        /// Gets the description.
        /// </summary>
        string Description { get; }

        /// <summary>
        /// Gets or sets the position of the parameter. Only relevant for positional parameters.
        /// </summary>
        int Position { get; set; }

        /// <summary>
        /// Gets the value type of the parameter.
        /// </summary>
        Type ValueType { get; }

        /// <summary>
        /// Gets or sets the basic argument, if any was found in the last parsing.
        /// </summary>
        IArgument BasicArgument { get; set; }

        /// <summary>
        /// Gets a value indicating if this parameter accepts a list of values in its arguments.
        /// </summary>
        bool IsListParameter { get; }

        /// <summary>
        /// Creates an argument based on the specified parameters and attaches it to this parameter.
        /// </summary>
        /// <param name="rawValue">The raw value of the argument.</param>
        /// <param name="value">The value of the argument.</param>
        /// <returns>An instance of <see cref="IArgument"/>.</returns>
        IArgument AttachArgument(string rawValue, object value);
    }
}
