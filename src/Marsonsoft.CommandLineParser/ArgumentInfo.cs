﻿using System.Collections.Generic;
using System.Linq;

namespace Marsonsoft.CommandLineParser
{
    internal class ArgumentInfo
    {
        public string Name { get; set; }

        public string Value { get; set; }

        public List<string> Values { get; set; }

        public string RawValue { get; set; }

        public IParameter Parameter { get; set; }

        public bool PassedAsShortName { get; set; }

        public bool IsNameless()
        {
            return Name == null;
        }
    }
}
