﻿using System;
using System.Collections.ObjectModel;

namespace Marsonsoft.CommandLineParser
{
    /// <summary>
    /// Defines the setup of the <see cref="Parser"/> class.
    /// </summary>
    public class ParserSetup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ParserSetup"/> class.
        /// </summary>
        public ParserSetup()
        {
            const string UniversalListSeparator = ",";

            string currentCultureSeparator = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator;
            ListSeparators.Add(UniversalListSeparator);
            if (currentCultureSeparator != UniversalListSeparator)
            {
                ListSeparators.Add(currentCultureSeparator);
            }
        }

        /// <summary>
        /// Gets or sets the parameter token.
        /// </summary>
        /// <value>
        /// The parameter token is used to mark a named command-line parameter. Default value: "--"
        /// </value>
        public string ParameterToken { get; set; } = "--";

        /// <summary>
        /// Gets or sets the short parameter token.
        /// </summary>
        /// <value>
        /// The parameter token is used to mark a single-letter command-line parameter. Default value: "-"
        /// </value>
        public string ShortParameterToken { get; set; } = "-";

        /// <summary>
        /// Gets or sets the assignment token.
        /// </summary>
        /// <value>
        /// The assignment token is used to separate a parameter name from it's value. Default value: "="
        /// </value>
        public string AssignmentToken { get; set; } = "=";

        /// <summary>
        /// Gets or sets the string comparison method. Default value: StringComparison.CurrentCulture.
        /// </summary>
        public StringComparison StringComparison { get; set; } = StringComparison.CurrentCulture;

        /// <summary>
        /// Gets a collection containing the list of strings that can be used as separators for options that can
        /// accept multiple values. By default, it contains the "," string and the current culture's list separator string (if not ",").
        /// </summary>
        public Collection<string> ListSeparators { get; } = new Collection<string>();

        /// <summary>
        /// Creates an instance of the <see cref="ParserSetup"/> class that supports Linux-style command-line arguments.
        /// </summary>
        /// <remarks>
        /// Linux-style command-line arguments example:
        /// <code>
        /// ls -F --color=auto /etc/xinetd.d/
        /// </code>
        /// </remarks>
        /// <returns>An instance of the <see cref="ParserSetup"/> class</returns>
        public static ParserSetup CreateLinuxStyle()
        {
            return new ParserSetup();
        }

        /// <summary>
        /// Creates an instance of the <see cref="ParserSetup"/> class that supports Windows-style command-line arguments.
        /// </summary>
        /// <remarks>
        /// Windows-style command-line arguments example:
        /// <code>
        /// csc /define:DEBUG /optimize /out:File2.exe *.cs
        /// </code>
        /// </remarks>
        /// <returns>An instance of the <see cref="ParserSetup"/> class</returns>
        public static ParserSetup CreateWindowsStyle()
        {
            return new ParserSetup
            {
                ParameterToken = "/",
                ShortParameterToken = null,
                AssignmentToken = ":",
                StringComparison = StringComparison.CurrentCultureIgnoreCase,
            };
        }

        /// <summary>
        /// Creates an instance of the <see cref="ParserSetup"/> class that supports Powershell-style command-line arguments.
        /// </summary>
        /// <remarks>
        /// Powershell-style command-line arguments example:
        /// <code>
        /// New-PSDrive -PSProvider FileSystem -Root C:\ -Name Sys
        /// </code>
        /// </remarks>
        /// <returns>An instance of the <see cref="ParserSetup"/> class</returns>
        public static ParserSetup CreatePowershellStyle()
        {
            return new ParserSetup
            {
                ParameterToken = "-",
                ShortParameterToken = null,
                AssignmentToken = null,
                StringComparison = StringComparison.CurrentCultureIgnoreCase,
            };
        }
    }
}
