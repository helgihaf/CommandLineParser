﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace Marsonsoft.CommandLineParser
{
    /// <summary>
    /// Represents a command-line parameter for list valued arguments.
    /// </summary>
    /// <typeparam name="T">The type of the elements in the list.</typeparam>
    /// <seealso cref="Marsonsoft.CommandLineParser.ParameterBase{T}" />
    public class ListParameter<T> : ParameterBase<T>
    {
        /// <summary>
        /// Gets or sets the argument, if any was found in the last parsing.
        /// </summary>
        public ListArgument<T> Argument { get; set; }

        /// <summary>
        /// Gets or sets the basic argument, if any was found in the last parsing.
        /// </summary>
        public override IArgument BasicArgument
        {
            get => Argument;
            set => Argument = (ListArgument<T>)value;
        }

        /// <summary>
        /// Returns true, indicating that this parameter accepts a list of values in its arguments.
        /// </summary>
        public override bool IsListParameter => true;

        /// <summary>
        /// Creates an argument based on the specified parameters and attaches it to the parameter.
        /// </summary>
        /// <param name="rawValue">The raw value of the argument.</param>
        /// <param name="value">The value of the argument.</param>
        /// <returns>
        /// An instance of <see cref="T:Marsonsoft.CommandLineParser.IArgument" />.
        /// </returns>
        public override IArgument AttachArgument(string rawValue, object value)
        {
            List<T> list = null;
            if (value != null)
            {
                list = new List<T>();

                if (value is System.Collections.IEnumerable enumerable && !(value is string))
                {
                    foreach (object item in enumerable)
                    {
                        list.Add(GetNativeValue(item));
                    }
                }
                else
                {
                    list.Add(GetNativeValue(value));
                }
            }

            var currentListArgument = Argument as ListArgument<T>;

            if (currentListArgument == null || currentListArgument.Value == null)
            {
                Argument = new ListArgument<T>(this, rawValue, list);
            }
            else if (list != null)
            {
                // TODO: rawValue is lost for this kind of argument, will we need it?
                currentListArgument.Value.AddRange(list);
            }

            return Argument;
        }
    }
}
