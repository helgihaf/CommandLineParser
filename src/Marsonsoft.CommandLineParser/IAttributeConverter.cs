﻿using System.Collections.Generic;

namespace Marsonsoft.CommandLineParser
{
    internal interface IAttributeConverter<T>
    {
        IEnumerable<IParameter> Parameters { get; }

        T CreateSubject();
    }
}