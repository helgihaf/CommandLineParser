﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace Marsonsoft.CommandLineParser
{
    internal class AttributeConverter<T> : IAttributeConverter<T>
    {
        private List<Item> items;
        private List<IParameter> parameters;

        public IEnumerable<IParameter> Parameters
        {
            get
            {
                if (items == null)
                {
                    items = CreateItems().ToList();
                    parameters = items.Select(i => i.Parameter).ToList();
                }

                return parameters;
            }
        }

        public T CreateSubject()
        {
            if (items == null)
            {
                throw new InvalidOperationException("No parameter information has been generated.");
            }

            T subject = Activator.CreateInstance<T>();
            foreach (var item in items)
            {
                if (item.Parameter.BasicArgument != null)
                {
                    if (IsCollection(item.PropertyInfo.PropertyType))
                    {
                        SetCollectionProperty(subject, item);
                    }
                    else
                    {
                        item.PropertyInfo.SetValue(subject, item.Parameter.BasicArgument.ValueObject);
                    }
                }
            }

            return subject;
        }

        private static void SetCollectionProperty(T subject, Item item)
        {
            if (item.PropertyInfo.SetMethod != null)
            {
                item.PropertyInfo.SetValue(subject, item.Parameter.BasicArgument.ValueObject);
            }
            else
            {
                var values = (System.Collections.IEnumerable)item.Parameter.BasicArgument.ValueObject;
                if (values == null)
                {
                    return;
                }

                object propertyCollection = item.PropertyInfo.GetValue(subject);
                var method = propertyCollection.GetType().GetTypeInfo().GetDeclaredMethod("Add");
                if (method == null)
                {
                    throw new ArgumentException(string.Format(
                        CultureInfo.InvariantCulture,
                        "The {0} property can not be set and no Add method was found to add items to the collection.",
                        item.PropertyInfo.Name));
                }

                foreach (object obj in values)
                {
                    method.Invoke(propertyCollection, new[] { obj });
                }
            }
        }

        private static Type GetListParameterType(PropertyInfo propertyInfo)
        {
            Type itemType = GetItemTypeOfGeneric(propertyInfo.PropertyType);
            var listParameterType = typeof(ListParameter<>);
            Type[] typeArgs = { itemType };
            return listParameterType.MakeGenericType(typeArgs);
        }

        private static Type GetParameterType(PropertyInfo propertyInfo)
        {
            Type itemType = propertyInfo.PropertyType;
            var parameterType = typeof(Parameter<>);
            Type[] typeArgs = { itemType };
            return parameterType.MakeGenericType(typeArgs);
        }

        private static Type GetItemTypeOfGeneric(Type propertyType)
        {
            return propertyType.GenericTypeArguments[0];
        }

        private static bool IsCollection(Type type)
        {
            if (!type.GetTypeInfo().IsGenericType)
            {
                return false;
            }

            var collectionTypeInfo = typeof(ICollection<>).GetTypeInfo();
            var typeInterfaces = type.GetTypeInfo().ImplementedInterfaces;
            return typeInterfaces.Any(t => t.GetTypeInfo().GUID == collectionTypeInfo.GUID);
        }

        private static IEnumerable<Item> CreateItems()
        {
            Type type = typeof(T);
            var propertyInfos = type.GetTypeInfo().DeclaredProperties;
            foreach (var propertyInfo in propertyInfos)
            {
                var parameterAttribute = propertyInfo.GetCustomAttribute<ParameterAttribute>(true);
                if (parameterAttribute == null)
                {
                    continue;
                }

                Type parameterType;
                if (IsCollection(propertyInfo.PropertyType))
                {
                    parameterType = GetListParameterType(propertyInfo);
                }
                else
                {
                    parameterType = GetParameterType(propertyInfo);
                }

                var parameter = Activator.CreateInstance(parameterType) as IInternalParameter;
                if (parameterAttribute.Aliases != null)
                {
                    parameter.Aliases = new List<string>(parameterAttribute.Aliases);
                }
                parameter.Name = parameterAttribute.Name;
                parameter.Description = parameterAttribute.Description;
                parameter.Position = parameterAttribute.Position;

                var item = new Item
                {
                    Parameter = (IParameter)parameter,
                    PropertyInfo = propertyInfo,
                    Attribute = parameterAttribute
                };
                yield return item;
            }
        }

        private class Item
        {
            public IParameter Parameter { get; set; }

            public PropertyInfo PropertyInfo { get; set; }

            public ParameterAttribute Attribute { get; set; }
        }
    }
}
