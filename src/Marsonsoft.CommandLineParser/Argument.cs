﻿namespace Marsonsoft.CommandLineParser
{
    /// <summary>
    /// Defines the argument value of a <see cref="IParameter"/> for non-list values.
    /// </summary>
    /// <typeparam name="T">The type of the argument.</typeparam>
    /// <remarks>
    /// NOTE: For list values, <see cref="ListParameter{T}"/>.
    /// </remarks>
    /// <seealso cref="ArgumentBase" />
    /// <seealso cref="ListParameter{T}"/>
    public class Argument<T> : ArgumentBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Argument{T}"/> class.
        /// </summary>
        /// <param name="parameter">The parameter that the argument belongs to.</param>
        /// <param name="rawValue">The raw value of the argument.</param>
        /// <param name="value">The value of the argument.</param>
        public Argument(IParameter parameter, string rawValue, T value)
            : base(parameter, rawValue)
        {
            Value = value;
        }

        /// <summary>
        /// Gets the typed value of the argument.
        /// </summary>
        public T Value { get; private set; }

        /// <summary>
        /// Gets the value of the argument as an <see cref="object" />.
        /// </summary>
        public override object ValueObject
        {
            get { return Value; }
        }
    }
}
