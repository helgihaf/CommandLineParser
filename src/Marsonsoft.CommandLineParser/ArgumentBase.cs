﻿namespace Marsonsoft.CommandLineParser
{
    /// <summary>
    /// A base class for implementing <see cref="IArgument"/>.
    /// </summary>
    /// <seealso cref="IArgument" />
    public abstract class ArgumentBase : IArgument
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ArgumentBase"/> class.
        /// </summary>
        /// <param name="parameter">The parameter that the argument belongs to.</param>
        /// <param name="rawValue">The raw value of the argument.</param>
        protected ArgumentBase(IParameter parameter, string rawValue)
        {
            Parameter = parameter;
            RawValue = rawValue;
        }

        /// <summary>
        /// Gets the parameter that this argument belongs to.
        /// </summary>
        public IParameter Parameter { get; private set; }

        /// <summary>
        /// Gets the raw value of the argument.
        /// </summary>
        public string RawValue { get; private set; }

        /// <summary>
        /// Gets the value of the argument as an <see cref="object" />.
        /// </summary>
        public abstract object ValueObject { get; }
    }
}
