﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;

namespace Marsonsoft.CommandLineParser
{
    /// <summary>
    /// Defines a command-line parser capable of parsing command-line arguments based on a <see cref="ParserSetup"/>.
    /// </summary>
    public class Parser
    {
        private readonly ParserSetup setup;
        private ParameterCollection parameterCollection;

        /// <summary>
        /// Initializes a new instance of the <see cref="Parser"/> class.
        /// </summary>
        /// <param name="setup">The setup of the parser.</param>
        /// <exception cref="System.ArgumentNullException">setup</exception>
        public Parser(ParserSetup setup)
        {
            if (setup == null)
            {
                throw new ArgumentNullException(nameof(setup));
            }
            ValidateSetup(setup);
            this.setup = setup;
        }

        /// <summary>
        /// Parses the specified command-line arguments and returns their values.
        /// </summary>
        /// <typeparam name="T">The type of the class that holds the parameter definitions accepted by the parser.</typeparam>
        /// <param name="args">The arguments to parse.</param>
        /// <returns>An instance of <typeparamref name="T"/>, holding values for the arguments that were parsed.</returns>
        public T Parse<T>(string[] args)
        {
            var attributeConverter = new AttributeConverter<T>();
            Parse(attributeConverter.Parameters, args);
            return attributeConverter.CreateSubject();
        }

        /// <summary>
        /// Parses the specified command-line arguments and puts their values into the specified parameters.
        /// </summary>
        /// <param name="parameters">The parameters accepted by the parser.</param>
        /// <param name="args">The arguments to parse.</param>
        /// <exception cref="ArgumentNullException">parameters</exception>
        /// <exception cref="System.ArgumentException">No <see cref="IParameter"/> object was found for an argument.</exception>
        public void Parse(IEnumerable<IParameter> parameters, string[] args)
        {
            if (parameters == null)
            {
                throw new ArgumentNullException(nameof(parameters));
            }

            parameterCollection = new ParameterCollection(parameters.ToList(), setup.StringComparison, !string.IsNullOrEmpty(setup.ShortParameterToken));
            parameterCollection.AutoAssignPositions();
            parameterCollection.ClearArguments();
            ValidateParameterCollection(parameterCollection);

            if (args == null)
            {
                return;
            }

            Debug.WriteLine(string.Join(" ", args));

            IList<ArgumentInfo> argumentInfos = ParseArguments(args);
            SetDefaultValues(argumentInfos);
        }

        private static void ValidateSetup(ParserSetup setup)
        {
            if (setup.ParameterToken == string.Empty)
            {
                throw new ArgumentException($"{nameof(setup.ParameterToken)} cannot be empty.", nameof(setup));
            }

            if (setup.ShortParameterToken == string.Empty)
            {
                throw new ArgumentException($"{nameof(setup.ShortParameterToken)} cannot be empty.", nameof(setup));
            }

            if (setup.ParameterToken != null && setup.ShortParameterToken != null && string.Equals(setup.ParameterToken, setup.ShortParameterToken))
            {
                throw new ArgumentException($"{nameof(setup.ParameterToken)} and {nameof(setup.ShortParameterToken)} cannot have the same non-null value.", nameof(setup));
            }

            if (setup.AssignmentToken == string.Empty)
            {
                throw new ArgumentException($"{nameof(setup.AssignmentToken)} cannot be empty.", nameof(setup));
            }
        }

        private void ValidateParameterCollection(ParameterCollection parameterCollection)
        {
            ISet<int> positions = new HashSet<int>();
            IParameter namelessListParameter = null;
            for (int i = 0; i < parameterCollection.Count; i++)
            {
                var parameter = parameterCollection[i];
                if (positions.Contains(parameter.Position))
                {
                    throw new ArgumentException($"There is more than one parameter that has position {parameter.Position}.");
                }

                if (parameter.IsListParameter && parameter.Name == null)
                {
                    if (namelessListParameter == null)
                    {
                        namelessListParameter = parameter;
                    }
                    else
                    {
                        throw new ArgumentException("There can only be one nameless list parameter defined.");
                    }
                }
            }
        }

        private static void SetDefaultValues(IList<ArgumentInfo> argumentInfos)
        {
            foreach (var argumentInfo in argumentInfos)
            {
                var parameter = argumentInfo.Parameter;
                if (parameter != null)
                {
                    if (!parameter.IsListParameter && parameter.BasicArgument.ValueObject == null)
                    {
                        parameter.AttachArgument(argumentInfo.RawValue, GetDefaultParameterValue(parameter.ValueType));
                    }
                }
            }
        }

        //private static bool IsListParameter(Type parameterType)
        //{
        //    return parameterType != null && parameterType.GetGenericTypeDefinition() == typeof(ListParameter<>);
        //}

        private static void AssignValues(ArgumentInfo info, List<string> values)
        {
            info.Value = null;
            info.Values = null;

            if (values.Count == 0)
            {
                info.Value = string.Empty;
            }
            else if (values.Count == 1)
            {
                info.Value = values[0];
            }
            else
            {
                info.Values = values;
            }
        }

        private static object GetDefaultParameterValue(Type t)
        {
            if (t == typeof(string))
            {
                return string.Empty;
            }
            else if (t == typeof(bool))
            {
                return true;
            }
            else if (!t.IsByRef)
            {
                return Activator.CreateInstance(t);
            }

            return null;
        }

        private static bool ParameterNeedsArgument(IParameter parameter)
        {
            return
                parameter != null &&
                parameter.BasicArgument?.ValueObject == null &&
                parameter.ValueType != typeof(bool);
        }

        private IList<ArgumentInfo> ParseArguments(string[] args)
        {
            int position = 0;
            IParameter previousParameter = null;
            var argumentInfos = ParseArgumentInfos(args);
            foreach (var argumentInfo in argumentInfos)
            {
                if (argumentInfo.IsNameless() && ParameterNeedsArgument(previousParameter))
                {
                    string rawValue = string.Join(" ", previousParameter.BasicArgument?.RawValue, argumentInfo.RawValue);
                    previousParameter.AttachArgument(rawValue, argumentInfo.Value ?? (object)argumentInfo.Values);
                }
                else
                {
                    var parameter = parameterCollection.GetParameter(argumentInfo, ref position);
                    if (parameter == null)
                    {
                        throw new ArgumentException(string.Format(CultureInfo.InvariantCulture, "No parameter found for argument {0}", argumentInfo.RawValue));
                    }

                    parameter.AttachArgument(argumentInfo.RawValue, argumentInfo.Value ?? (object)argumentInfo.Values);
                    previousParameter = parameter;
                    argumentInfo.Parameter = parameter;
                }
            }

            return argumentInfos;
        }

        private IList<ArgumentInfo> ParseArgumentInfos(string[] args)
        {
            var list = new List<ArgumentInfo>();
            for (int i = 0; i < args.Length; i++)
            {
                list.AddRange(ParseArgument(args[i]));
            }

            return list;
        }

        private IEnumerable<ArgumentInfo> ParseArgument(string arg)
        {
            if (string.IsNullOrWhiteSpace(arg))
            {
                return new ArgumentInfo[] { };
            }

            ArgumentInfo argumentInfo = null;

            argumentInfo = ParseParameterToken(arg);
            if (argumentInfo != null)
            {
                return new ArgumentInfo[] { argumentInfo };
            }

            var argumentInfos = ParseShortParameterToken(arg);
            if (argumentInfos.Any())
            {
                return argumentInfos;
            }

            argumentInfo = new ArgumentInfo
            {
                RawValue = arg,
            };
            ParseValuesInto(arg, 0, argumentInfo);

            return new ArgumentInfo[] { argumentInfo };
        }

        private IEnumerable<ArgumentInfo> ParseShortParameterToken(string argument)
        {
            if (string.IsNullOrEmpty(setup.ShortParameterToken))
            {
                return new ArgumentInfo[] { };
            }

            int shortTokenLength = setup.ShortParameterToken.Length;

            if (argument.Length <= shortTokenLength || !argument.StartsWith(setup.ShortParameterToken, setup.StringComparison))
            {
                return new ArgumentInfo[] { };
            }

            string names = argument.Substring(shortTokenLength);
            if (names.Length == 1)
            {
                return new[]
                {
                    new ArgumentInfo
                    {
                        RawValue = argument,
                        Name = names,
                        PassedAsShortName = true
                    }
                };
            }

            // Either names are a combined list of flags or a short token with an attached argument.
            if (IsCombinedFlags(names))
            {
                var argumentInfos = new List<ArgumentInfo>();
                foreach (char c in names)
                {
                    argumentInfos.Add(new ArgumentInfo
                    {
                        RawValue = argument,
                        Name = c.ToString(),
                        PassedAsShortName = true
                    });
                }
                return argumentInfos.ToArray();
            }
            else
            {
                var value = argument.Substring(shortTokenLength + 1);
                return new[]
                {
                    new ArgumentInfo
                    {
                        RawValue = argument,
                        Name = names[0].ToString(),
                        Value = value,
                        PassedAsShortName = true
                    }
                };

            }
        }

        private bool IsCombinedFlags(string names)
        {
            // If first char is a bool parameter then all the rest of them must be as well.
            string first = names[0].ToString();
            return parameterCollection.Any(p => p.ValueType == typeof(bool) && (p.Name == first || p.Aliases != null && p.Aliases.Any(a => a == first)));
        }

        private ArgumentInfo ParseParameterToken(string argument)
        {
            if (string.IsNullOrEmpty(setup.ParameterToken))
            {
                return null;
            }

            int tokenLength = setup.ParameterToken.Length;

            if (argument.Length <= tokenLength || !argument.StartsWith(setup.ParameterToken, setup.StringComparison))
            {
                return null;
            }

            string argumentRest = argument.Substring(tokenLength);

            int index = -1;
            if (!string.IsNullOrEmpty(setup.AssignmentToken))
            {
                index = argumentRest.IndexOf(setup.AssignmentToken, setup.StringComparison);
            }

            if (index == -1)
            {
                return new ArgumentInfo
                {
                    RawValue = argument,
                    Name = argumentRest
                };
            }

            var result = new ArgumentInfo
            {
                RawValue = argument,
                Name = argumentRest.Substring(0, index)
            };

            ParseValuesInto(argumentRest, index + setup.AssignmentToken.Length, result);
            return result;
        }

        private void ParseValuesInto(string s, int startIndex, ArgumentInfo info)
        {
            var values = ParseValues(s, startIndex);
            AssignValues(info, values);
        }

        private List<string> ParseValues(string s, int startIndex)
        {
            var values = new List<string>();
            if (startIndex < s.Length)
            {
                values.AddRange(ParseValues(s.Substring(startIndex)));
            }

            return values;
        }

        private IEnumerable<string> ParseValues(string values)
        {
            return values.Split(setup.ListSeparators.ToArray(), StringSplitOptions.None);
        }
    }
}
