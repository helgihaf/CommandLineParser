﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marsonsoft.CommandLineParser
{
    internal sealed class ParameterNameIndices
    {
        private readonly bool handleShortNames;
        private readonly IDictionary<string, IParameter> nameIndex;
        private readonly IDictionary<string, IParameter> shortNameIndex;

        public ParameterNameIndices(bool handleShortNames, StringComparer stringComparer)
        {
            this.handleShortNames = handleShortNames;
            nameIndex = new Dictionary<string, IParameter>(stringComparer);
            if (handleShortNames)
            {
                shortNameIndex = new Dictionary<string, IParameter>(stringComparer);
            }
        }

        public void Add(string name, IParameter parameter)
        {
            var dictionary = SelectDictionary(name);
            dictionary.Add(name, parameter);
        }

        public IParameter GetByNameOrDefault(string name)
        {
            nameIndex.TryGetValue(name, out IParameter parameter);
            return parameter;
        }

        public IParameter GetByShortNameOrDefault(string name)
        {
            if (!handleShortNames)
            {
                throw new InvalidOperationException("Cannot get by short name when not handling short names in indices.");
            }
            shortNameIndex.TryGetValue(name, out IParameter parameter);
            return parameter;
        }

        private IDictionary<string, IParameter> SelectDictionary(string name)
        {
            IDictionary<string, IParameter> result;

            if (handleShortNames && IsShortName(name))
            {
                result = shortNameIndex;
            }
            else
            {
                result = nameIndex;
            }

            return result;
        }

        private static bool IsShortName(string name)
        {
            return name != null && name.Length == 1;
        }
    }
}
