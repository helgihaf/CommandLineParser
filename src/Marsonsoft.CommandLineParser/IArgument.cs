﻿namespace Marsonsoft.CommandLineParser
{
    /// <summary>
    /// Defines the argument of a <see cref="IParameter"/>.
    /// </summary>
    public interface IArgument
    {
        /// <summary>
        /// Gets the parameter that this argument belongs to.
        /// </summary>
        IParameter Parameter { get; }

        /// <summary>
        /// Gets the raw value of the argument.
        /// </summary>
        string RawValue { get; }

        /// <summary>
        /// Gets the value of the argument as an <see cref="object"/>.
        /// </summary>
        object ValueObject { get; }
    }
}
