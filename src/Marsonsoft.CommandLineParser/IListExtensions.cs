﻿using System.Collections.Generic;

namespace Marsonsoft.CommandLineParser
{
    internal static class IListExtensions
    {
        public static void AddRange<T>(this IList<T> list, IEnumerable<T> items)
        {
            if (items == null)
            {
                return;
            }

            foreach (var item in items)
            {
                list.Add(item);
            }
        }
    }
}
