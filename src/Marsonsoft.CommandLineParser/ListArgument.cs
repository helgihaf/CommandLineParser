﻿using System.Collections.Generic;

namespace Marsonsoft.CommandLineParser
{
    /// <summary>
    /// Defines the argument value of a <see cref="IParameter"/> for list values.
    /// </summary>
    /// <typeparam name="T">The type of the elements in the list.</typeparam>
    /// <seealso cref="Marsonsoft.CommandLineParser.ArgumentBase" />
    public class ListArgument<T> : ArgumentBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ListArgument{T}"/> class.
        /// </summary>
        /// <param name="parameter">The parameter that the argument belongs to.</param>
        /// <param name="rawValue">The raw value of the argument.</param>
        /// <param name="value">The value.</param>
        public ListArgument(IParameter parameter, string rawValue, IList<T> value)
            : base(parameter, rawValue)
        {
            Value = value;
        }

        /// <summary>
        /// Gets the value of the argument.
        /// </summary>
        public IList<T> Value { get; private set; }

        /// <summary>
        /// Gets the value of the argument as an <see cref="object" />.
        /// </summary>
        public override object ValueObject
        {
            get { return Value; }
        }
    }
}
