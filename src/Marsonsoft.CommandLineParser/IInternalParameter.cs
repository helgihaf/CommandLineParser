﻿using System.Collections.Generic;

namespace Marsonsoft.CommandLineParser
{
    internal interface IInternalParameter
    {
        string Name { get; set; }

        IEnumerable<string> Aliases { get; set; }

        string Description { get; set; }

        int Position { get; set; }
    }
}
