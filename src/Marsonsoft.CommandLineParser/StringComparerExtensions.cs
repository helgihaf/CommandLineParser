﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marsonsoft.CommandLineParser
{
    internal class StringComparerExtensions
    {
        public static StringComparer FromStringComparison(StringComparison stringComparison)
        {
            switch (stringComparison)
            {
                case StringComparison.CurrentCulture:
                    return StringComparer.CurrentCulture;
                case StringComparison.CurrentCultureIgnoreCase:
                    return StringComparer.CurrentCultureIgnoreCase;
                case StringComparison.Ordinal:
                    return StringComparer.Ordinal;
                case StringComparison.OrdinalIgnoreCase:
                    return StringComparer.OrdinalIgnoreCase;
                default:
                    return StringComparer.CurrentCulture;
            }
        }
    }
}
