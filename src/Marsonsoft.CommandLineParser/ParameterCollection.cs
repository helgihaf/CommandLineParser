﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Marsonsoft.CommandLineParser
{
    internal class ParameterCollection : Collection<IParameter>
    {
        private readonly StringComparison nameStringComparison;
        private readonly bool handleShortNames;

        private ParameterNameIndices indices;

        public ParameterCollection(IList<IParameter> list, StringComparison nameStringComparison, bool handleShortNames)
            : base(list)
        {
            this.nameStringComparison = nameStringComparison;
            this.handleShortNames = handleShortNames;
        }

        public void ClearArguments()
        {
            foreach (var parameter in this)
            {
                parameter.BasicArgument = null;
            }
        }

        public void AutoAssignPositions()
        {
            if (Count == 0)
            {
                return;
            }

            int nextPosition = this.Max(p => p.Position) + 1;
            for (int i = 0; i < Count; i++)
            {
                if (IsUniassignedPositional(this[i]))
                {
                    this[i].Position = nextPosition++;
                }
            }
        }

        private bool IsUniassignedPositional(IParameter parameter)
        {
            return parameter.Name == null && (parameter.Aliases == null || !parameter.Aliases.Any()) && parameter.Position == -1;
        }

        public IParameter GetParameter(ArgumentInfo argumentInfo, ref int position)
        {
            IParameter parameter = null;

            if (argumentInfo.Name != null)
            {
                if (indices == null)
                {
                    indices = BuildIndices();
                }

                if (!argumentInfo.PassedAsShortName)
                {
                    parameter = indices.GetByNameOrDefault(argumentInfo.Name);
                }
                else
                {
                    parameter = indices.GetByShortNameOrDefault(argumentInfo.Name);
                }
            }
            else
            {
                var pos = position;
                parameter = this.FirstOrDefault(p => p.Position == pos);
                if (parameter != null && !parameter.IsListParameter)
                {
                    position++;
                }
            }

            return parameter;
        }

        protected override void ClearItems()
        {
            base.ClearItems();
            indices = null;
        }

        protected override void InsertItem(int index, IParameter item)
        {
            base.InsertItem(index, item);
            indices = null;
        }

        protected override void RemoveItem(int index)
        {
            base.RemoveItem(index);
            indices = null;
        }

        protected override void SetItem(int index, IParameter item)
        {
            base.SetItem(index, item);
            indices = null;
        }

        private ParameterNameIndices BuildIndices()
        {
            var indices = new ParameterNameIndices(handleShortNames, StringComparerExtensions.FromStringComparison(nameStringComparison));
            foreach (var parameter in this)
            {
                if (parameter.Name != null)
                {
                    indices.Add(parameter.Name, parameter);
                    if (parameter.Aliases != null)
                    {
                        foreach (var alias in parameter.Aliases)
                        {
                            indices.Add(alias, parameter);
                        }
                    }
                }
            }

            return indices;
        }
    }
}
