﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace Marsonsoft.CommandLineParser
{
    /// <summary>
    /// A base class for command line parameters.
    /// </summary>
    /// <typeparam name="T">The value type of the parameter.</typeparam>
    /// <seealso cref="Marsonsoft.CommandLineParser.IParameter" />
    public abstract class ParameterBase<T> : IParameter, IInternalParameter
    {
        /// <summary>
        /// Gets or sets the unique name of the parameter.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the optional aliases of the parameter.
        /// </summary>
        public IEnumerable<string> Aliases { get; set; }

        /// <summary>
        /// Gets or sets the description of the parameter.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the position of the parameter. Only relevant for positional parameters.
        /// </summary>
        public int Position { get; set; } = -1;

        /// <summary>
        /// Gets or sets the basic argument, if any was found in the last parsing.
        /// </summary>
        public abstract IArgument BasicArgument { get; set; }

        /// <summary>
        /// Gets the value type of the parameter.
        /// </summary>
        public Type ValueType
        {
            get
            {
                return typeof(T);
            }
        }

        /// <summary>
        /// Gets a value indicating if this parameter accepts a list of values in its arguments.
        /// </summary>
        public virtual bool IsListParameter => false;

        /// <summary>
        /// Gets the display name of the parameter.
        /// </summary>
        protected string DisplayName
        {
            get
            {
                return Name ?? Aliases?.FirstOrDefault() ?? string.Format(CultureInfo.InvariantCulture, "at position {0}", Position);
            }
        }

        /// <summary>
        /// Creates an argument based on the specified parameters.
        /// </summary>
        /// <param name="rawValue">The raw value of the argument.</param>
        /// <param name="value">The value of the argument.</param>
        /// <returns>
        /// An instance of <see cref="IArgument" />.
        /// </returns>
        public abstract IArgument AttachArgument(string rawValue, object value);

        /// <summary>
        /// Gets the native value of the specified object.
        /// </summary>
        /// <param name="value">The object value.</param>
        /// <returns>An instance of type T created from <paramref name="value"/>.</returns>
        protected T GetNativeValue(object value)
        {
            Type targetType = typeof(T);

            // If T is a nullable value, we drill one down to the concrete type
            Type underlyingType = Nullable.GetUnderlyingType(targetType);
            if (underlyingType != null)
            {
                targetType = underlyingType;
            }

            if (value == null)
            {
                if (targetType == typeof(bool))
                {
                    return (T)((object)true);
                }

                return default(T);
            }

            if (targetType.GetTypeInfo().IsAssignableFrom(value.GetType().GetTypeInfo()))
            {
                return (T)((object)value);
            }

            // If we had the System.ComponentModel.TypeConverter class in this version of .NET Standard, we wouldn't need
            // the following code.
            if (targetType == typeof(bool))
            {
                return (T)((object)Convert.ToBoolean(value));
            }
            else if (targetType == typeof(string))
            {
                return (T)((object)Convert.ToString(value));
            }
            else if (targetType == typeof(int))
            {
                return (T)((object)Convert.ToInt32(value));
            }
            else if (targetType == typeof(uint))
            {
                return (T)((object)Convert.ToUInt32(value));
            }
            else if (targetType == typeof(byte))
            {
                return (T)((object)Convert.ToByte(value));
            }
            else if (targetType == typeof(sbyte))
            {
                return (T)((object)Convert.ToSByte(value));
            }
            else if (targetType == typeof(short))
            {
                return (T)((object)Convert.ToInt16(value));
            }
            else if (targetType == typeof(ushort))
            {
                return (T)((object)Convert.ToUInt16(value));
            }
            else if (targetType == typeof(long))
            {
                return (T)((object)Convert.ToInt64(value));
            }
            else if (targetType == typeof(ulong))
            {
                return (T)((object)Convert.ToUInt64(value));
            }
            else if (targetType == typeof(decimal))
            {
                return (T)((object)Convert.ToDecimal(value));
            }
            else if (targetType == typeof(float))
            {
                return (T)((object)Convert.ToSingle(value));
            }
            else if (targetType == typeof(double))
            {
                return (T)((object)Convert.ToDouble(value));
            }
            else if (targetType == typeof(char))
            {
                return (T)((object)Convert.ToChar(value));
            }
            else if (targetType == typeof(DateTime))
            {
                return (T)((object)Convert.ToDateTime(value));
            }
            else
            {
                throw new NotImplementedException(string.Format(CultureInfo.InvariantCulture,
                    "Cannot convert a value of type {0} to type {1}.", value.GetType().FullName, typeof(T).FullName));
            }
        }
    }
}
