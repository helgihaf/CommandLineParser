﻿using Marsonsoft.CommandLineParser;
using System;

namespace SampleCore
{
    internal static class ReadIntoParameterObjects
    {
        public static void Run(ParserSetup parserSetup, string[] args)
        {
            var parser = new Parser(parserSetup);

            var versionParam = new Parameter<bool>
            {
                Name = "version",
                Description = "Output version information and exit."
            };
            var whitespaceParam = new ListParameter<int>
            {
                Name = "whitespace",
                Aliases = new[] { "w" },
                Description = "Comma-separated list of characters to treat as whitespace in decimal, example: 8,9,10,11,12,13"
            };
            var outputParam = new Parameter<string>
            {
                Name = "output",
                Aliases = new[] { "o" },
                Description = "Specify output file (instead of stdout)."
            };
            var dateTimeFilterParam = new Parameter<DateTime?>
            {
                Name = "dateTimeFilter",
                Aliases = new [] { "d" },
                Description = "Some filter accepting a date/time value."
            };
            var inputFilesParam = new ListParameter<string>
            {
                Description = "Input file(s)"
            };
            var parameters = new IParameter[]
            {
                versionParam,
                whitespaceParam,
                outputParam,
                dateTimeFilterParam,
                inputFilesParam
            };

            parser.Parse(parameters, args);

            Console.WriteLine("Parse into parameter objects results: ------------");

            Console.WriteLine($"Version: {versionParam.Argument?.Value}");

            string whitespace = whitespaceParam.Argument != null ? string.Join(",", whitespaceParam.Argument.Value) : "missing";
            Console.WriteLine($"Whitespace: {whitespace}");

            Console.WriteLine($"OutputFilePath: {outputParam.Argument?.Value}");
            Console.WriteLine($"DateTimeFilter: {dateTimeFilterParam.Argument?.Value}");

            string inputFiles = inputFilesParam.Argument != null ? string.Join(",", inputFilesParam.Argument.Value) : "missing";
            Console.WriteLine($"InputFilePaths: {inputFiles}");
        }
    }
}
