﻿using Marsonsoft.CommandLineParser;
using System;

namespace SampleCore
{
    class Program
    {
        static void Main(string[] args)
        {
            //var parserSetup = ParserSetup.CreateLinuxStyle();
            //var parserSetup = ParserSetup.CreatePowershellStyle();
            var parserSetup = ParserSetup.CreateWindowsStyle();

            ReadIntoClass.Run(parserSetup, args);
            ReadIntoParameterObjects.Run(parserSetup, args);
        }
    }
}