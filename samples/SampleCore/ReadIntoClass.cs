﻿using Marsonsoft.CommandLineParser;
using System;
using System.Collections.Generic;

namespace SampleCore
{
    public class TokenizeCommandLine
    {
        [Parameter(Name = "version", Description = "Output version information and exit.")]
        public bool Version { get; set; }

        [Parameter(Name = "whitespace", Aliases = new[] { "w" }, Description = "Comma-separated list of characters to treat as whitespace in decimal, example: 8,9,10,11,12,13")]
        public List<int> Whitespace { get; set; }

        [Parameter(Name = "output", Aliases = new[] { "o" }, Description = "Specify output file (instead of stdout).")]
        public string OutputFilePath { get; set; }

        [Parameter(Name = "dateTimeFilter", Aliases = new[] { "d" }, Description = "Some filter accepting a date/time value.")]
        public DateTime? DateTimeFilter { get; set; }

        [Parameter(Description = "Input file(s)")]
        public List<string> InputFilePaths { get; set; }
    }

    internal static class ReadIntoClass
    {
        public static void Run(ParserSetup parserSetup, string[] args)
        {
            var parser = new Parser(parserSetup);

            var tokenizeCommandLine = parser.Parse<TokenizeCommandLine>(args);

            Console.WriteLine("Parse into class results: ------------");

            Console.WriteLine($"Version: {tokenizeCommandLine.Version}");
            Console.WriteLine($"Whitespace: {string.Join(",", tokenizeCommandLine?.Whitespace)}");
            Console.WriteLine($"OutputFilePath: {tokenizeCommandLine.OutputFilePath}");
            Console.WriteLine($"DateTimeFilter: {tokenizeCommandLine.DateTimeFilter}");
            Console.WriteLine($"InputFilePaths: {string.Join(",", tokenizeCommandLine?.InputFilePaths)}");
        }
    }
}